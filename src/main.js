import Vue from 'vue'
import App from './App'
import router from './router'
import './assets/css/global.css'
import { Button, Form, FormItem, Input } from 'element-ui'
import './assets/fonts/iconfont.css'
import axios from 'axios'
axios.default.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
Vue.prototype.$http = axios
Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)


Vue.config.productionTip = false


/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    render: h => h(App)
})