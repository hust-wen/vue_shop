import Vue from 'vue'
import Router from 'vue-router'
import Login from '../views/login.vue'
import Home from '../views/login.vue'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            redirect: '/login'

        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/Home',
            name: 'Home',
            component: Home
        }
    ]
})